/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/

#include "../ReadTriggerDecision.h"
#include "../RunTriggerMatching.h"
#include "../ReadxAOD.h"
#include "../WritexAOD.h"

DECLARE_COMPONENT(ReadTriggerDecision)
DECLARE_COMPONENT(RunTriggerMatching)
DECLARE_COMPONENT(ReadxAOD)
DECLARE_COMPONENT(WritexAOD)
