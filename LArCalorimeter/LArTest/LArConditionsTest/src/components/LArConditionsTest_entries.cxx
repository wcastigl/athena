/*
  Copyright (C) 2002-2025 CERN for the benefit of the ATLAS collaboration
*/
#include "LArConditionsTest/LArConditionsTestAlg.h"
#include "LArConditionsTest/LArCondDataTest.h"
#include "LArConditionsTest/LArCablingTest.h"
#include "../LArSCIdVsIdTest.h"
DECLARE_COMPONENT( LArConditionsTestAlg )
DECLARE_COMPONENT( LArCondDataTest )
DECLARE_COMPONENT( LArCablingTest )
DECLARE_COMPONENT( LArSCIdvsIdTest )
